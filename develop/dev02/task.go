package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"unicode"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	// Так как в условие четко не указано, из приведенных примеров сделаем предположение,
	// что во входных строках будут только латинские буквы нижнего регистра, цифры,
	// а количество повторений одного символа не превышает 9

	str, err := unpackString("a4bc2d5e")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(str)
}

func unpackString(s string) (string, error) {
	var result []rune
	runes := []rune(s)

	for i := 0; i < len(runes); i++ {
		if unicode.IsDigit(runes[i]) {
			return "", errors.New("invalid data")
		}

		if i < len(runes)-1 && unicode.IsDigit(runes[i+1]) {
			count, _ := strconv.Atoi(string(runes[i+1]))

			for j := 0; j < count; j++ {
				result = append(result, rune(runes[i]))
			}

			i++

			continue
		}

		result = append(result, runes[i])
	}

	return string(result), nil
}
