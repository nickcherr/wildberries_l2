package main

import "testing"

type testCase struct {
	Input   string
	Output  string
	IsError bool
}

func TestUnpack(t *testing.T) {
	testCases := []testCase{
		{"a4bc2d5e", "aaaabccddddde", false},
		{"abcd", "abcd", false},
		{"45", "", true},
		{"", "", false},
	}

	var isError bool

	for i, testCase := range testCases {
		output, err := unpackString(testCase.Input)
		if err == nil {
			isError = false
		} else {
			isError = true
		}

		if output != testCase.Output || isError != testCase.IsError {
			t.Fatalf("Error: testcase number %d", i)
		}
	}
}
