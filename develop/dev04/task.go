package main

import (
	"fmt"
	"sort"
	"strings"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	words := []string{"Тяпка", "пятка", "пятак", "пятка", "листок", "столик", "слиток", "колесо"}
	anagrams := findAnagrams(&words)
	fmt.Println(anagrams)
}

func sortWord(word string) string {
	runes := []rune(word)
	sort.Slice(runes, func(i, j int) bool {
		return runes[i] < runes[j]
	})
	return string(runes)
}

func getUniqueWords(sortedWords []string) []string {
	if len(sortedWords) <= 1 {
		return sortedWords
	}
	uniqueWords := []string{sortedWords[0]}
	prevWord := sortedWords[0]
	var word string

	for i := 1; i < len(sortedWords); i++ {
		word = sortedWords[i]
		if word != prevWord {
			prevWord = word
			uniqueWords = append(uniqueWords, word)
		}
	}

	return uniqueWords
}

func findAnagrams(words *[]string) map[string][]string {
	anagrams := map[string][]string{}
	sortedKeyToKey := map[string]string{}
	for _, word := range *words {
		word = strings.ToLower(word)
		sortedWord := sortWord(word)
		key, ok := sortedKeyToKey[sortedWord]
		if !ok {
			sortedKeyToKey[sortedWord] = word
			key = word
		}
		if _, ok := anagrams[key]; !ok {
			anagrams[key] = []string{word}
		} else {
			anagrams[key] = append(anagrams[key], word)
		}
	}

	for k, v := range anagrams {
		if len(v) <= 1 {
			delete(anagrams, k)
		} else {
			sort.Strings(anagrams[k])
			anagrams[k] = getUniqueWords(anagrams[k])
		}
	}

	return anagrams
}
