package main

import (
	"reflect"
	"testing"
)

type testCase struct {
	Input  []string
	Output map[string][]string
}

func TestFindAnagrams(t *testing.T) {
	testCases := []testCase{
		{
			[]string{"Тяпка", "пятка", "пятак", "пятка", "листок", "столик", "слиток", "колесо"},
			map[string][]string{
				"листок": {"листок", "слиток", "столик"},
				"тяпка":  {"пятак", "пятка", "тяпка"},
			},
		},
	}

	for i, testcase := range testCases {
		anagrams := findAnagrams(&testcase.Input)
		if !reflect.DeepEqual(anagrams, testcase.Output) {
			t.Fatalf("Failed at testcase number %d", i)
		}
	}
}
