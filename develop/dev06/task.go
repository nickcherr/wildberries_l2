package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type Parameters struct {
	FieldsDescription string
	Delimiter         string
	Separated         bool
}

var (
	ErrInvalidFieldsDecription = errors.New("invalid fields description")
)

func main() {
	parameters := parseArgs()

	fieldsIntervals, err := getFieldsIntervals(parameters.FieldsDescription)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		line := scanner.Text()
		if parameters.Separated && !strings.Contains(line, parameters.Delimiter) {
			continue
		}
		fields := getFields(line, parameters.Delimiter, fieldsIntervals)
		fmt.Println(fields)
	}
}

func getFields(line, delimiter string, fieldsIntervals [][2]int) string {
	fields := strings.Split(line, delimiter)
	var requiredFields strings.Builder
	var leftBorder int

	for _, interval := range fieldsIntervals {
		if interval[0] > len(fields) {
			continue
		}

		if interval[0] == 0 {
			leftBorder = 0
		} else {
			leftBorder = interval[0] - 1
		}

		if interval[1] > len(fields) || interval[1] == 0 {
			for i := leftBorder; i < len(fields); i++ {
				requiredFields.WriteString(fields[i])
			}
			continue
		}

		for i := leftBorder; i < interval[1]; i++ {
			requiredFields.WriteString(fields[i])
		}
	}

	return requiredFields.String()
}

func getFieldsIntervals(fieldsDescription string) ([][2]int, error) {
	if fieldsDescription == "" {
		fieldsIntervals := make([][2]int, 1)
		return fieldsIntervals, nil
	}

	intervalDescriptions := strings.Split(fieldsDescription, ",")
	fieldsIntervals := make([][2]int, len(intervalDescriptions))

	var leftBorder, rightBorder int
	var err error

	for i, description := range intervalDescriptions {
		interval := strings.Split(description, "-")

		if len(interval) > 2 {
			return [][2]int{}, ErrInvalidFieldsDecription
		}

		if interval[0] != "" {
			leftBorder, err = strconv.Atoi(interval[0])
			if err != nil {
				return [][2]int{}, ErrInvalidFieldsDecription
			}
		} else {
			leftBorder = 0
		}

		if len(interval) == 1 {
			rightBorder = leftBorder
		} else if interval[1] != "" {
			rightBorder, err = strconv.Atoi(interval[1])
			if err != nil {
				return [][2]int{}, ErrInvalidFieldsDecription
			}
		} else {
			rightBorder = 0
		}

		if leftBorder < 0 || leftBorder > rightBorder && rightBorder != 0 {
			return [][2]int{}, ErrInvalidFieldsDecription
		}

		fieldsIntervals[i][0] = leftBorder
		fieldsIntervals[i][1] = rightBorder
	}

	return fieldsIntervals, nil
}

func parseArgs() Parameters {
	parameters := Parameters{}

	flag.StringVar(&parameters.FieldsDescription, "f", "", "Select fields numbers")
	flag.StringVar(&parameters.Delimiter, "d", "\t", "Change separator (default is TAB)")
	flag.BoolVar(&parameters.Separated, "s", false, "Print only lines with delimiter")

	flag.Parse()

	return parameters
}
