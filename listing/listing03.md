Что выведет программа? Объяснить вывод программы. Объяснить внутреннее устройство интерфейсов и их отличие от пустых интерфейсов.

```go
package main

import (
	"fmt"
	"os"
)

func Foo() error {
	var err *os.PathError = nil
	return err
}

func main() {
	err := Foo()
	fmt.Println(err)
	fmt.Println(err == nil)
}
```

Ответ:
```
Программа выведет:
<nil>  
false  
Foo() возвращает указатель на значение os.PathError, который имеет значение nil.  
Но сравнение err == nil будет равно true только тогда, когда и значение err, и тип err равны nil.  
Это связано с работой интерфейсов.  

```
