Что выведет программа? Объяснить вывод программы.

```go
package main

type customError struct {
	msg string
}

func (e *customError) Error() string {
	return e.msg
}

func test() *customError {
	{
		// do something
	}
	return nil
}

func main() {
	var err error
	err = test()
	if err != nil {
		println("error")
		return
	}
	println("ok")
}
```

Ответ:
```
Программа выведет "error".  
Сравнение err == nil будет равно true только тогда, когда и значение err, и тип err равны nil.  
Это связано с работой интерфейсов.  
В нашем случае тип err - это customError, а не nil.  

```
