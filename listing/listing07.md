Что выведет программа? Объяснить вывод программы.

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for {
			select {
			case v := <-a:
				c <- v
			case v := <-b:
				c <- v
			}
		}
	}()
	return c
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4 ,6, 8)
	c := merge(a, b )
	for v := range c {
		fmt.Println(v)
	}
}
```

Ответ:
```
Программа выведет цифры от 1 до 8 в случайном порядке, но при этом цифры 1,3,5,7 будут между собой идти по порядку (1 раньше 3, 3 раньше 5, ...), аналогично 2,4,6,8 будут между собой выведены по порядку. Это связано с элементом случайности в работе оператора select и с тем, что в пределах одного канала значения читаются в том порядке, в котором они в него отправляются. Затем будут бесконечно выводиться нули, пока программа не будет прервана, так как из закрытого канала не возможно прочитать ничего, но при этом возвращается нулевое значение.  

```
